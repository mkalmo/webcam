package webcam;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamDevice;
import org.bridj.Platform;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Test {

	public static void main(String[] args) throws IOException, InterruptedException {

		String deviceName = Platform.isMacOSX() ? "0" : "/dev/video0";

		FFmpegCliDevice device = new FFmpegCliDevice("", deviceName, "640x480");

		device.open();

		Thread.sleep(2000);

		saveImage(device.getImage(), "i1.jpg");
		saveImage(device.getImage(), "i2.jpg");
		saveImage(device.getImage(), "i3.jpg");
		saveImage(device.getImage(), "i4.jpg");

		device.close();
	}

	private static void saveImage(BufferedImage image, String fileName) throws IOException {
		ImageIO.write(image, "JPG", new File(fileName));
	}


}
